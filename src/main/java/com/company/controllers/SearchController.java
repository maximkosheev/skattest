package com.company.controllers;

import com.company.domain.Operator;
import com.company.domain.dto.SearchOperatorResponseDto;
import com.company.services.OperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Locale;

@Controller
public class SearchController {
    private static final String FEEDBACK_MESSAGE_SUCCESS = "feedback.message.success";
    private static final String FEEDBACK_MESSAGE_ERROR_NOT_FOUND = "feedback.message.error.not_found";

    @Autowired
    private OperatorService operatorService;

    @Resource
    private MessageSource messageSource;

    @GetMapping(name = "/search")
    public String searchForm(Model model) {
        return "search";
    }

    @PostMapping(name = "/search", produces = "application/json;charset=utf-8")
    @ResponseBody
    public SearchOperatorResponseDto searchOperator(@RequestBody Operator operator) {
        Operator operatorFromDB = operatorService.findBySnils(operator.getSnils());

        SearchOperatorResponseDto responseBody = new SearchOperatorResponseDto();
        if (operatorFromDB != null) {
            responseBody.setSuccess(true);
            responseBody.setDescription(getMessage(FEEDBACK_MESSAGE_SUCCESS));
            responseBody.setId(operatorFromDB.getId());
            responseBody.setSurname(operatorFromDB.getFamily());
            responseBody.setName(operatorFromDB.getName());
            responseBody.setPatronymic(operatorFromDB.getLogname());
            responseBody.setPosition(String.valueOf(operatorFromDB.getPosition()));
            //responseBody.put("branch", ???);
            return responseBody;
        } else {
            responseBody.setSuccess(false);
            responseBody.setDescription(getMessage(FEEDBACK_MESSAGE_ERROR_NOT_FOUND));
            return responseBody;
        }
    }

    private String getMessage(String code, Object... params) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(code, params, locale);
    }
}
