package com.company.domain;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "operators")
@Data
public class Operator {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long GlobalKey;
    @Column(name = "id_operator")
    private Long id;
    @Column(name = "Family", length = 50)
    private String family;
    @Column(name = "Name", length = 50)
    private String name;
    @Column(name = "Farth", length = 50)
    private String farth;
    @Column(name = "pass")
    private String pass;
    @Column(name = "role")
    private short role;
    @Column(name = "dp_FIO", length = 150)
    private String dpFIO;
    @Column(name = "logname", length = 50)
    private String logname;
    @Column(name = "log_name", length = 50)
    private String log_name;
    @Column(name = "phone", length = 50)
    private String phone;
    @Column(name = "DOB")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date DOB;
    @Column(name = "picture", nullable = false)
    private String picture = "";
    @Column(name = "passport", length = 50)
    private String passport;
    @Column(name = "snils", length = 50)
    private String snils;
    @Column(name = "no_visible")
    private byte noVisible = 0;
    @Column(name = "id_otdel", nullable = false)
    private int otdelId = 186;
    @Column(name = "position", nullable = false)
    private short position = 0;
    @Column(name = "rabbit_status")
    private short rabbitStatus;
    @Column(name = "date_last_update")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastUpdatedDt = new Date();
    @Column(name = "Id_Otdel_Sync")
    private int otdelIdSync;
}
