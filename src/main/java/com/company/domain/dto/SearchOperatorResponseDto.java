package com.company.domain.dto;

import lombok.Data;

@Data
public class SearchOperatorResponseDto {
    private Boolean success;
    private String description;
    private Long id;
    private String surname;
    private String name;
    private String patronymic;
    private String position;
    private String branch;
}
