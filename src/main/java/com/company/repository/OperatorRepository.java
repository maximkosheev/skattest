package com.company.repository;

import com.company.domain.Operator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OperatorRepository extends JpaRepository<Operator, Long> {
    /**
     * Возвращает оператора по его СНИЛС'у
     * @param value СНИЛС
     * @return оператор
     */
    Operator findBySnils(String value);
}
