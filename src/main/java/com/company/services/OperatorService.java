package com.company.services;

import com.company.domain.Operator;

public interface OperatorService {
    /**
     * Возвращает оператора по его СНИЛС'у
     * @param snils - СНИЛС
     * @return оператор из базы данных
     */
    Operator findBySnils(String snils);
}
