package com.company.services.impl;

import com.company.domain.Operator;
import com.company.repository.OperatorRepository;
import com.company.services.OperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OperatorServiceImpl implements OperatorService {
    @Autowired
    private OperatorRepository repository;

    @Override
    public Operator findBySnils(String snils) {
        return repository.findBySnils(snils);
    }
}
