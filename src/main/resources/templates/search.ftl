<#import "parts/common.ftl" as c>
<@c.page>
<h3>Страница поиска</h3>
<form id="search-form" action="/search" method="post">
    <div class="form-group">
        <label for="snils">СНИЛС</label>
        <input id="snils"  class="form-control" name="snils" value="" placeholder="СНИЛС">
    </div>
    <button id="search-btn" type="button" class="btn btn-primary">Поиск</button>
</form>

<div id="search-result">
    <div id="result-description"></div>
    <div id="result-data"></div>
</div>

<script type="text/javascript">
    $('#search-btn').on('click', function () {
        $.ajax({
            type: "POST",
            url: "/seach",
            data: JSON.stringify({snils: $('#snils').val()}),
            dataType: "json",
            contentType : "application/json",
            success: function(data){
                try {
                    $('#result-description').html(data.description);
                    if (data.success == true) {
                        $('#result-data').html(JSON.stringify(data));
                    }
                } catch (e) {

                }
            }
        });
    })
</script>
</@c.page>