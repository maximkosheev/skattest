package com.company.controllers;

import com.company.domain.Operator;
import com.company.domain.dto.SearchOperatorResponseDto;
import com.company.services.OperatorService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class SearchControllerIntegrationTest {
    private MockMvc mockMvc;

    @Mock
    private MessageSource messageSource;

    @Mock
    private OperatorService operatorService;

    @InjectMocks
    private SearchController searchController;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(searchController)
                .build();
    }

    @Test
    public void checkControllerNotNullTest() throws Exception {
        assertNotNull(searchController);
    }

    @Test
    public void searchSuccessTest() throws Exception {
        Operator operator = new Operator();
        operator.setSnils("108-003-636 06");
        operator.setId(2222L);

        when(operatorService.findBySnils("108-003-636 06")).thenReturn(operator);

        mockMvc.perform(post("search")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(asJsonString(operator)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.success", is(true)))
                .andExpect(jsonPath("$.id", is(2222)));
        verify(operatorService, times(1)).findBySnils("108-003-636 06");
        verifyNoMoreInteractions(operatorService);
    }

    @Test
    public void searchFailedTest() throws Exception {
        Operator operator = new Operator();
        operator.setSnils("108-003-636 10");

        when(operatorService.findBySnils("108-003-636 10")).thenReturn(null);

        mockMvc.perform(post("search")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(operator)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.success", is(false)));
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
